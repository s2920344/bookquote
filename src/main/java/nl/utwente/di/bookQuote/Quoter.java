package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        double res = 0;
        switch (isbn) {
            case "1" -> res = 10.0;
            case "2" -> res = 45.0;
            case "3" -> res = 20.0;
            case "4" -> res = 35.0;
            case "5" -> res = 50.0;
            default -> res = 0.0;
        }

        return res;
    }
}
